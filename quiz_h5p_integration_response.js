/**
 * @file
 * File quiz_h5p_integration_response.js manages H5P answers display.
 *
 * @category Quiz Response
 *
 * @package Quiz H5P Integration
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.behaviors.quizH5PIntegrationResponse = {
    attach: function (context, settings) {

      /**
      * Main function. Triggered when document is loaded.
      */
      var init = function () {
        if (H5P && $.isArray(H5P.instances)) {
          $.each(H5P.instances, function (instanceIndex, instance) {
            if (instance.contentId) {
              var H5PContentId = instance.contentId;
              if (instance.params && instance.params.userAnswers) {
                if ($.isArray(instance.params.userAnswers)) {
                  // First, we load the user answers.
                  $.each(instance.params.userAnswers, function (answerIndex, answer) {
                    var $quizH5PAnswerField = $('#quiz-h5p-integration-' + H5PContentId + '-answer-' + answerIndex);
                    if ($quizH5PAnswerField.length) {
                      instance.params.userAnswers[answerIndex] = $quizH5PAnswerField.val();
                      if (instance.clozes && instance.clozes[answerIndex]) {
                        instance.clozes[answerIndex].setUserInput(instance.params.userAnswers[answerIndex]);
                        instance.clozes[answerIndex].checkAnswer();
                      }
                    }
                  });
                  // Then, we load H5P field evaluation.
                  $.each(instance.params.userAnswers, function (answerIndex, answer) {
                    if (instance.markResults && instance.showEvaluation) {
                      instance.markResults();
                      instance.showEvaluation();
                    }
                  });
                }
              }
            }
          });
        }
      };

      // Triggering init() function when document is loaded.
      $(window).load(function () {
        init();
      });
    }
  };

  // Run drupal's attachBehaviors() just to be sure that quizH5PIntegrationResponse behavior is loaded.
  Drupal.attachBehaviors(document, Drupal.settings);

})(jQuery, Drupal, this, this.document);
