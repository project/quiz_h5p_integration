<?php

/**
 * @file
 * QuizH5PIntegrationQuestion.class.php file.
 *
 * @category Quiz Question
 *
 * @package Quiz H5P Integration
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

/**
 * QuizH5PIntegrationQuestion class.
 *
 * Extension of QuizQuestion.
 * Inherits or implements code found in quiz_question.core.inc.
 */
class QuizH5PIntegrationQuestion extends QuizQuestion {

  /**
   * Constructor.
   *
   * @param object $node
   *   The node object.
   *
   * @override
   *   QuizQuestion constructor stores the node object.
   */
  public function __construct(stdClass &$node) {
    parent::__construct($node);

    if (empty($node->main_library_id)) {
      if (function_exists('h5p_node_info') && function_exists('h5p_load')) {
        if (!empty($this->node->nid)) {
          h5p_load(array($this->node->nid => &$this->node));
        }
      }
    }

    if (empty($this->node->revision)) {
      $this->node->revision = 0;
    }
  }

  /**
   * Implementation of getCreationForm.
   *
   * @see QuizQuestion#getCreationForm()
   */
  public function getCreationForm(array &$form_state = NULL) {
    $form = array();

    if (function_exists('h5p_node_info') && function_exists('h5p_form')) {
      $form = h5p_form($this->node, $form_state);
      if (function_exists('h5peditor_form_h5p_content_node_form_alter')) {
        $form['#node'] = $this->node;
        h5peditor_form_h5p_content_node_form_alter($form, $form_state);
      }
    }

    // Get the nodes settings, users settings or default settings.
    $default_settings = $this->getSettings();

    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#description' => t('Your settings will be remembered.'),
      '#weight' => 30,
    );

    $form['settings']['choice_boolean'] = array(
      '#type' => 'checkbox',
      '#title' => t('Simple scoring'),
      '#description' => t('Give max score if everything is correct. Zero points otherwise.'),
      '#default_value' => $default_settings['choice_boolean'],
      '#parents' => array('choice_boolean'),
    );

    return $form;
  }

  /**
   * Implementation of validate.
   *
   * QuizQuestion#validate()
   */
  public function validateNode(array &$form) {
    if (function_exists('h5p_node_info') && function_exists('h5p_validate')) {
      h5p_validate($this->node, $form);
    }
  }

  /**
   * Helper function provding the default settings for the creation form.
   *
   * @return array
   *   Array with the default settings
   */
  private function getSettings() {
    // If the node is being updated the default settings
    // are those stored in the node.
    if (isset($this->node->nid) && isset($this->node->choice_boolean)) {
      $settings['choice_boolean'] = $this->node->choice_boolean;
    }
    // We try to fetch the users settings.
    elseif ($settings = $this->getUserSettings()) {
    }
    // The user is creating his first quiz h5p node.
    else {
      $settings['choice_boolean'] = 0;
    }
    return $settings;
  }

  /**
   * Fetches the user default settings for the creation form.
   *
   * @return mixed
   *   The user default node settings
   */
  private function getUserSettings() {
    global $user;
    $res = db_query('SELECT choice_boolean
            FROM {quiz_h5p_integration_user_settings}
            WHERE uid = :uid', array(':uid' => $user->uid))->fetchAssoc();
    if ($res) {
      return $res;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Implementation of save.
   *
   * Stores the question in the database.
   *
   * @param bool $is_new
   *   If the node is a new node.
   *
   * @see sites/all/modules/quiz-HEAD/question_types/quiz_question/QuizQuestion#save()
   */
  public function saveNodeProperties($is_new = FALSE) {
    // Get the node settings, user settings or default settings.
    $settings = $this->getSettings();

    $is_new = $is_new || $this->node->revision == 1;

    if ($is_new) {
      db_insert('quiz_h5p_integration_properties')
        ->fields(array(
          'nid' => $this->node->nid,
          'vid' => $this->node->vid,
          'choice_boolean' => $settings['choice_boolean'],
        ))
        ->execute();

      $this->node->valid_answers = $this->getH5pValidAnswers();
      // TODO: utilize the benefit of multiple insert of DBTNG.
      if (!empty($this->node->valid_answers)) {
        for ($i = 0; isset($this->node->valid_answers[$i]); $i++) {
          if (drupal_strlen($this->node->valid_answers[$i]['answer']['value']) > 0) {
            $this->insertAnswer($i);
          }
        }
      }
    }
    else {
      db_update('quiz_h5p_integration_properties')
        ->fields(array(
          'choice_boolean' => $settings['choice_boolean'],
        ))
        ->condition('nid', $this->node->nid)
        ->condition('vid', $this->node->vid)
        ->execute();

      $updated_answers = $this->getH5pValidAnswers();

      if (empty($this->node->valid_answers)) {
        $this->node->valid_answers = $this->getH5pValidAnswers();
      }
      else {
        foreach ($this->node->valid_answers as $i => &$current_answer) {
          if (is_array($updated_answers) && !empty($updated_answers[$i])) {
            if ($current_answer['answer']['value'] != $updated_answers[$i]['answer']['value']) {
              $current_answer['answer']['value'] = $updated_answers[$i]['answer']['value'];
              $current_answer['feedback']['value'] = $updated_answers[$i]['feedback']['value'];
            }
          }
        }

        $i = empty($i) ? 0 : $i + 1;
        for ($i; isset($updated_answers[$i]); $i++) {
          $this->node->valid_answers[] = $updated_answers[$i];
        }
      }

      // We fetch ids for the existing answers belonging to this question.
      // We need to figure out if an existing answer has been changed
      // or deleted.
      $existing_answers = db_query('SELECT id FROM {quiz_h5p_integration_answers}
              WHERE question_nid = :nid AND question_vid = :vid', array(':nid' => $this->node->nid, ':vid' => $this->node->vid));

      // We start by assuming that all existing answers needs to be deleted.
      $ids_to_delete = array();
      while ($existing_answer = $existing_answers->fetch()) {
        $ids_to_delete[] = $existing_answer->id;
      }

      for ($i = 0; isset($this->node->valid_answers[$i]); $i++) {
        $answer = $this->node->valid_answers[$i];
        if (drupal_strlen($this->node->valid_answers[$i]['answer']['value']) > 0) {
          // If new answer.
          if (empty($answer['id']) || !is_numeric($answer['id'])) {
            $this->insertAnswer($i);
          }
          // If existing alternative.
          else {
            $this->updateAnswer($i);
            // Make sure this alternative isn't deleted.
            $key = array_search($answer['id'], $ids_to_delete);
            $ids_to_delete[$key] = FALSE;
          }
        }
      }
      foreach ($ids_to_delete as $id_to_delete) {
        if ($id_to_delete) {
          db_delete('quiz_h5p_integration_answers')
            ->condition('id', $id_to_delete)
            ->execute();
        }
      }

      // Update max_score for relationships if auto update max score
      // is enabled for question.
      // This has to be done here because quiz module doesn't update
      // relationship table correctly.
      $quizzes_to_update = array();
      $result = db_query(
        'SELECT parent_vid as vid from {quiz_node_relationship} where child_nid = :nid and child_vid = :vid and auto_update_max_score=1', array(':nid' => $this->node->nid, ':vid' => $this->node->vid));
      foreach ($result as $record) {
        $quizzes_to_update[] = $record->vid;
      }

      db_update('quiz_node_relationship')
        ->fields(array('max_score' => $this->getMaximumScore()))
        ->condition('child_nid', $this->node->nid)
        ->condition('child_vid', $this->node->vid)
        ->condition('auto_update_max_score', 1)
        ->execute();

      quiz_update_max_score_properties($quizzes_to_update);

    }
    $this->saveUserSettings();
  }

  /**
   * Helper function. Saves a new answer.
   *
   * @param int $i
   *   The answer index.
   */
  private function insertAnswer($i) {
    $answer = $this->normalizeAnswer($this->node->valid_answers[$i]);
    db_insert('quiz_h5p_integration_answers')
      ->fields(array(
        'answer' => $answer['answer'],
        'answer_format' => $answer['answer_format'],
        'feedback' => $answer['feedback'],
        'feedback_format' => $answer['feedback_format'],
        'score' => $answer['score'],
        'question_nid' => $this->node->nid,
        'question_vid' => $this->node->vid,
        'weight' => isset($answer['weight']) ? $answer['weight'] : $i,
      ))
      ->execute();
  }

  /**
   * Helper function. Updates an existing answer.
   *
   * @param int $i
   *   The answer index.
   */
  private function updateAnswer($i) {
    $answer = $this->normalizeAnswer($this->node->valid_answers[$i]);
    db_update('quiz_h5p_integration_answers')
      ->fields(array(
        'answer' => $answer['answer'],
        'answer_format' => $answer['answer_format'],
        'feedback' => $answer['feedback'],
        'feedback_format' => $answer['feedback_format'],
        'score' => $answer['score'],
        'question_nid' => $this->node->nid,
        'question_vid' => $this->node->vid,
        'weight' => isset($answer['weight']) ? $answer['weight'] : $i,
      ))
      ->condition('id', $answer['id'])
      ->condition('question_nid', $this->node->nid)
      ->condition('question_vid', $this->node->vid)
      ->execute();
  }

  /**
   * Helper function. normalize answer array.
   *
   * @param array $answer
   *   The answer array.
   *
   * @return array
   *   The normalized answer array
   */
  private function normalizeAnswer(array $answer) {
    $normalized_answer = array(
      'score'  => NULL,
      'weight'  => NULL,
      'answer'  => NULL,
      'answer_format'  => NULL,
      'feedback'  => NULL,
      'feedback_format' => NULL,
    );
    if (isset($answer['id'])) {
      $normalized_answer['id'] = $answer['id'];
    }
    if (isset($answer['score'])) {
      $normalized_answer['score'] = $answer['score'];
    }
    if (isset($answer['weight'])) {
      $normalized_answer['weight'] = $answer['weight'];
    }
    // answer.
    if (is_array($answer['answer'])) {
      if (isset($answer['answer']['value'])) {
        $normalized_answer['answer'] = $answer['answer']['value'];
      }
      if (isset($answer['answer']['format'])) {
        $normalized_answer['answer_format'] = $answer['answer']['format'];
      }
    }
    // feedback.
    if (is_array($answer['feedback'])) {
      if (isset($answer['feedback']['value'])) {
        $normalized_answer['feedback'] = $answer['feedback']['value'];
      }
      if (isset($answer['feedback']['format'])) {
        $normalized_answer['feedback_format'] = $answer['feedback']['format'];
      }
    }
    return $normalized_answer;
  }

  /**
   * Helper function. get H5P valid answers.
   *
   * @return array
   *   The H5P answer array
   */
  private function getH5pValidAnswers() {
    if (empty($this->node->json_content)) {
      return array();
    }
    else {
      $json_content = json_decode($this->node->json_content);
      if (empty($json_content->questions)) {
        return array();
      }
      else {
        if (is_array($json_content->questions)) {
          $answers_found = array();
          foreach ($json_content->questions as $question) {
            $matches = array();
            preg_match_all('#((\*)([^\*]+)(\*))#i', $question, $matches);
            if (is_array($matches) && !empty($matches[0])) {
              foreach ($matches[0] as $answers) {
                $valid_answers = explode('/', trim($answers, "* \t\n\r\0\x0B"));
                if (count($valid_answers)) {
                  $or = t('or');
                  $answers_found[] = array(
                    'answer' => array(
                      'value' => json_encode($valid_answers),
                      'format' => 'json',
                    ),
                    'feedback' => array(
                      'value' => htmlentities('«' . implode('» ' . $or . ' «', $valid_answers) . '»', ENT_QUOTES, 'UTF-8'),
                      'format' => 'filtered_html',
                    ),
                    'score' => 1,
                  );
                }
              }
            }
          }
          return $answers_found;
        }
        else {
          return array();
        }
      }
    }
  }

  /**
   * Saves the user settings from the creation form.
   */
  private function saveUserSettings() {
    // Get the node settings, user settings or default settings.
    $settings = $this->getSettings();

    global $user;
    db_merge('quiz_h5p_integration_user_settings')
      ->key(array('uid' => $user->uid))
      ->fields(array(
        'choice_boolean' => $settings['choice_boolean'],
      ))
      ->execute();
  }

  /**
   * Implementation of getNodeProperties.
   *
   * @see QuizQuestion#getNodeProperties()
   */
  public function getNodeProperties() {
    if (isset($this->nodeProperties) && !empty($this->nodeProperties)) {
      return $this->nodeProperties;
    }
    $node_properties = parent::getNodeProperties();

    if (empty($node_properties['max_score'])) {
      $node_properties['max_score'] = $this->getMaximumScore();
    }

    $properties = db_query('SELECT choice_boolean FROM {quiz_h5p_integration_properties}
            WHERE nid = :nid AND vid = :vid', array(':nid' => $this->node->nid, ':vid' => $this->node->vid))->fetchAssoc();

    if (is_array($properties)) {
      $node_properties = array_merge($node_properties, $properties);
    }

    // Load the valid answers.
    $answers = db_query('SELECT id, answer, answer_format, feedback, feedback_format, score, weight
            FROM { quiz_h5p_integration_answers}
            WHERE question_nid = :question_nid AND question_vid = :question_vid
            ORDER BY weight', array(':question_nid' => $this->node->nid, ':question_vid' => $this->node->vid));

    // Init array so it can be iterated even if empty.
    $node_properties['valid_answers'] = array();

    while ($answer = $answers->fetchAssoc()) {
      $node_properties['valid_answers'][] = array(
        'id' => $answer['id'],
        'answer' => array(
          'value' => $answer['answer'],
          'format' => $answer['answer_format'],
        ),
        'feedback' => array(
          'value' => $answer['feedback'],
          'format' => $answer['feedback_format'],
        ),
        'score' => $answer['score'],
        'weight' => $answer['weight'],
      );
    }

    $this->nodeProperties = $node_properties;
    return $node_properties;
  }

  /**
   * Implementation of getMaximumScore.
   *
   * @see QuizQuestion#getMaximumScore()
   */
  public function getMaximumScore($ignore_choice = FALSE) {
    $max_score = 1;
    if (empty($this->node->choice_boolean) || !$this->node->choice_boolean || $ignore_choice) {
      if (empty($this->node->json_content)) {
        return $max_score;
      }
      else {
        $max_score = $this->getH5pMaxScoreFromJson($this->node->json_content);
        return $max_score;
      }
    }
    else {
      return $max_score;
    }
  }

  /**
   * Helper function. get H5P max score.
   *
   * @return int
   *   The H5P max score
   */
  private function getH5pMaxScoreFromJson($json = NULL) {
    $default_max_score = 1;
    if (empty($json)) {
      return $default_max_score;
    }
    else {
      $json_content = json_decode($json);
      if (empty($json_content->questions)) {
        return $default_max_score;
      }
      else {
        if (is_array($json_content->questions)) {
          $detected_max_score = 0;
          foreach ($json_content->questions as $question) {
            $matches = array();
            $detected_max_score += preg_match_all('#((\*)([^\*]+)(\*))#i', $question, $matches);
          }
          if ($detected_max_score > $default_max_score) {
            return $detected_max_score;
          }
          else {
            return $default_max_score;
          }
        }
        else {
          return $default_max_score;
        }
      }
    }
  }

  /**
   * Implementation of delete.
   *
   * @see QuizQuestion#delete()
   */
  public function delete($only_this_version = FALSE) {
    $delete_properties = db_delete('quiz_h5p_integration_properties')->condition('nid', $this->node->nid);
    $delete_answers = db_delete('quiz_h5p_integration_answers')->condition('question_nid', $this->node->nid);

    if ($only_this_version) {
      $delete_properties->condition('vid', $this->node->vid);
      $delete_answers->condition('question_vid', $this->node->vid);
    }

    $delete_properties->execute();
    $delete_answers->execute();

    if (function_exists('h5p_node_info')) {
      if ($only_this_version) {
        if (function_exists('h5p_node_revision_delete')) {
          h5p_node_revision_delete($this->node);
        }
      }
      else {
        if (function_exists('h5p_delete')) {
          h5p_delete($this->node);
        }
      }
    }

    parent::delete($only_this_version);
  }

  /**
   * Implementation of getNodeView.
   *
   * @see QuizQuestion#getNodeView()
   */
  public function getNodeView() {
    if (function_exists('h5p_node_info') && function_exists('h5p_view')) {
      h5p_view($this->node, 'full');
    }

    $content = parent::getNodeView();
    if (!empty($this->node->answers) && !empty($this->node->answers[0]) && !empty($this->node->answers[0]['answer'])) {
      $user_answers = json_decode($this->node->answers[0]['answer']);
      if (!empty($user_answers) && is_array($user_answers)) {
        $content['quiz_h5p_integration_user_answers'][] = array();

        for ($i = 0; isset($user_answers[$i]); $i++) {
          $content['quiz_h5p_integration_user_answers'][$i] = array(
            '#type' => 'hidden',
            '#value' => $user_answers[$i],
            '#attributes' => array(
              'readonly' => NULL,
              'id' => 'quiz-h5p-integration-' . h5p_get_content_id($this->node) . '-answer-' . $i,
              'class' => 'quiz-h5p-integration-answer quiz-h5p-integration-' . h5p_get_content_id($this->node) . '-answer',
            ),
          );
        }

        $content['#attached']['js'][] = array(
          'type' => 'file',
          'cache' => FALSE,
          'data' => drupal_get_path('module', 'quiz_h5p_integration') . '/quiz_h5p_integration_response.js',
        );

      }
    }
    return $content;
  }

  /**
   * Generates the question element.
   *
   * This is called whenever a question is rendered, either
   * to an administrator or to a quiz taker.
   */
  public function getAnsweringForm(array $form_state = NULL, $result_id = NULL) {
    $element = parent::getAnsweringForm($form_state, $result_id);

    if (function_exists('h5p_node_info') && function_exists('h5p_view')) {
      h5p_view($this->node, 'full');
      if (function_exists('h5p_add_files_and_settings')) {
        $integration = h5p_add_files_and_settings(NULL, NULL);
        if ($integration) {
          drupal_add_js(array(
            'quizH5PIntegration' => array(
              'H5PIntegration' => $integration,
            ),
          ), 'setting');
        }
      }
    }

    if (isset($result_id)) {
      // This question has already been answered. We load the answer.
      $response = new QuizH5PIntegrationResponse($result_id, $this->node);
    }

    $element['quiz_h5p_integration_user_score'] = array(
      '#type' => 'hidden',
      '#default_value' => NULL,
      '#attributes' => array(
        'id' => 'quiz-h5p-integration-' . h5p_get_content_id($this->node) . '-score',
        'class' => 'quiz-h5p-integration-answer quiz-h5p-integration-' . h5p_get_content_id($this->node) . '-score',
      ),
    );

    if (!empty($this->node->valid_answers)) {
      $element['quiz_h5p_integration_user_answers'][] = array();

      if (isset($response)) {
        $user_answers = $response->getResponse();
        if (!empty($user_answers) && is_string($user_answers)) {
          $user_answers = json_decode($user_answers);
        }
      }

      for ($i = 0; isset($this->node->valid_answers[$i]); $i++) {
        $element['quiz_h5p_integration_user_answers'][$i] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'id' => 'quiz-h5p-integration-' . h5p_get_content_id($this->node) . '-answer-' . $i,
            'class' => 'quiz-h5p-integration-answer quiz-h5p-integration-' . h5p_get_content_id($this->node) . '-answer',
          ),
        );
        if (isset($user_answers) && is_array($user_answers) && isset($user_answers[$i])) {
          $element['quiz_h5p_integration_user_answers'][$i]['#value'] = $user_answers[$i];
        }
      }
    }

    $element['#attached']['js'][] = array(
      'type' => 'file',
      'cache' => FALSE,
      'data' => drupal_get_path('module', 'quiz_h5p_integration') . '/quiz_h5p_integration_question.js',
    );

    return $element;
  }

  /**
   * Question response validator.
   */
  public function getAnsweringFormValidate(array &$element, &$values) {
    if (isset($values['quiz_h5p_integration_user_score']) && is_numeric($values['quiz_h5p_integration_user_score'])) {
      if (!empty($this->node->valid_answers)) {
        if ($values['quiz_h5p_integration_user_score'] > count($this->node->valid_answers)) {
          form_error($element['quiz_h5p_integration_user_score'], t('You must provide an answer.'));
          return FALSE;
        }
      }
    }
    else {
      form_error($element['quiz_h5p_integration_user_score'], t('You must provide an answer.'));
      return FALSE;
    }

    if (!empty($this->node->valid_answers)) {
      if (isset($values['quiz_h5p_integration_user_answers']) && is_array($values['quiz_h5p_integration_user_answers'])) {
        array_walk($values['quiz_h5p_integration_user_answers'], 'trim');
        $values['quiz_h5p_integration_user_answers'] = array_filter($values['quiz_h5p_integration_user_answers']);
        if (empty($values['quiz_h5p_integration_user_answers'])) {
          form_error($element['quiz_h5p_integration_user_answers'], t('You must provide an answer.'));
          return FALSE;
        }
        if (count($this->node->valid_answers) != count($values['quiz_h5p_integration_user_answers'])) {
          form_error($element['quiz_h5p_integration_user_answers'], t('You must provide an answer.'));
          return FALSE;
        }
      }
      else {
        form_error($element['quiz_h5p_integration_user_answers'], t('You must provide an answer.'));
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Implementation of save.
   *
   * @see QuizQuestion#save()
   */
  public function save($is_new = FALSE) {
    if (function_exists('h5p_node_info') && function_exists('h5p_update')) {
      h5p_update($this->node);
      if (function_exists('h5peditor_node_update')) {
        h5peditor_node_update($this->node);
      }
    }
    parent::save($is_new);
  }

}
