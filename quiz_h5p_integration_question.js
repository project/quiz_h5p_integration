/**
 * @file
 * File quiz_h5p_integration_question.js manages H5P questions display.
 *
 * @category Quiz Question
 *
 * @package Quiz H5P Integration
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

(function ($, Drupal, window, document) {

  'use strict';

  if (!$.isNumeric) {
    // Polyfill for older jQuery version.
    $.isNumeric = function (value) {
      return !isNaN(parseFloat(value));
    };
  }

  /**
  * Loads user answers into H5P fields if any found.
  */
  var loadUserAnswers = function () {
    if (window.H5P && $.isArray(window.H5P.instances)) {
      $.each(window.H5P.instances, function (instanceIndex, instance) {
        if (instance.contentId) {
          var H5PContentId = instance.contentId;
          if (instance.params && instance.params.userAnswers) {
            if ($.isArray(instance.params.userAnswers)) {
              // We load the user answers.
              $.each(instance.params.userAnswers, function (answerIndex, answer) {
                var $quizH5PAnswerField = $('#quiz-h5p-integration-' + H5PContentId + '-answer-' + answerIndex);
                if ($quizH5PAnswerField.length) {
                  instance.params.userAnswers[answerIndex] = $quizH5PAnswerField.val();
                  if (instance.clozes && instance.clozes[answerIndex]) {
                    instance.clozes[answerIndex].setUserInput(instance.params.userAnswers[answerIndex]);
                  }
                }
              });
            }
          }
        }
      });
    }
  };

  /**
  * Updates quiz user answer hidden fields with H5P user answers.
  */
  var updateQuizH5PAnswerFields = function () {
    if (window.H5P && $.isArray(window.H5P.instances)) {
      $.each(window.H5P.instances, function (instanceIndex, instance) {
        if (instance.contentId) {
          var H5PContentId = instance.contentId;
          if (instance.params && instance.params.userAnswers) {
            if ($.isArray(instance.params.userAnswers)) {
              $.each(instance.params.userAnswers, function (answerIndex, answer) {
                var $quizH5PAnswerField = $('#quiz-h5p-integration-' + H5PContentId + '-answer-' + answerIndex);
                if ($quizH5PAnswerField.length) {
                  $quizH5PAnswerField.val(answer);
                }
              });
            }
          }
        }
      });
    }
  };

  /**
  * Updates quiz user score hidden field with H5P user score.
  *
  * @param {number} currentContentId (optional) the H5P content which score has to be extracted and applied to the hidden field.
  * @param {number} score (optional) the score to set as field value.
  */
  var updateQuizH5PScoreField = function (currentContentId, score) {
    if (!$.isNumeric(score)) {
      if (window.H5P && $.isArray(window.H5P.instances)) {
        $.each(window.H5P.instances, function (instanceIndex, instance) {
          if (instance.contentId) {
            var H5PContentId = instance.contentId;
            if ($.isNumeric(currentContentId)) {
              if (H5PContentId === currentContentId) {
                if (typeof instance.getScore === 'function') {
                  score = instance.getScore();
                }
                return false;
              }
            }
            else {
              if (typeof instance.getScore === 'function') {
                score = instance.getScore();
                if ($.isNumeric(score)) {
                  var $quizH5PScoreField = $('#quiz-h5p-integration-' + H5PContentId + '-score');
                  if ($quizH5PScoreField.length) {
                    $quizH5PScoreField.val(score);
                  }
                }
              }
            }
          }
        });
      }
    }

    if ($.isNumeric(currentContentId) && $.isNumeric(score)) {
      var $quizH5PScoreField = $('#quiz-h5p-integration-' + currentContentId + '-score');
      if ($quizH5PScoreField.length) {
        $quizH5PScoreField.val(score);
      }
    }
  };

  /**
  * Sets quiz user score hidden field user-score
  * attribute with H5P user score.
  *
  * @param {number} currentContentId the H5P content which score has to be extracted and applied to the hidden field.
  * @param {string} subContentId the H5P subcontent which score has to be extracted and applied to the hidden field.
  * @param {number} score the user score.
  * @param {number} maxScore the max score.
  */
  var setUserScoreMetadata = function (currentContentId, subContentId, score, maxScore) {
    if ($.isNumeric(currentContentId) && $.isNumeric(score) && $.isNumeric(maxScore) && subContentId) {
      var $quizH5PScoreField = $('#quiz-h5p-integration-' + currentContentId + '-score');
      if ($quizH5PScoreField.length) {
        var userScore = $quizH5PScoreField.attr('user-score');
        try {
          userScore = JSON.parse(userScore);
        }
        catch (e) {
          userScore = {};
        }
        userScore[subContentId] = {
          score: score,
          maxScore: maxScore
        };
        try {
          userScore = JSON.stringify(userScore);
        }
        catch (e) {
          userScore = '{}';
        }
        $quizH5PScoreField.attr('user-score', userScore);
      }
    }
  };

  /**
  * Main function. Triggered when DOM is ready.
  */
  var init = function () {
    if (window.H5P) {
      if (window.H5P.externalDispatcher) {
        // Each time an event is triggered on an H5P instance.
        window.H5P.externalDispatcher.on('xAPI', function (event) {
          event.preventBubbling();
          // Updates quiz user answer hidden fields.
          updateQuizH5PAnswerFields();

          // Gets the current score.
          var score = event.getScore();
          // Gets the current max score.
          var maxScore = event.getMaxScore();
          // Gets H5P content Id which triggered the event.
          var currentContentId = event.getVerifiedStatementValue(['object', 'definition', 'extensions', 'http://h5p.org/x-api/h5p-local-content-id']);
          var subContentId = event.getVerifiedStatementValue(['object', 'definition', 'extensions', 'http://h5p.org/x-api/h5p-subContentId']);

          // Sets the quiz user score hidden field user-data attribute.
          if (subContentId && event.getVerifiedStatementValue(['result', 'completion'])) {
            setUserScoreMetadata(currentContentId, subContentId, score, maxScore);
          }
          // Updates quiz user score hidden field.
          updateQuizH5PScoreField(currentContentId, score);
        });
      }

      // Updates quiz user answer hidden fields one last time before submitting the form.
      var $quizQuestionAnsweringForm = $('#quiz-question-answering-form');
      if ($quizQuestionAnsweringForm.length) {
        $quizQuestionAnsweringForm.submit(function () {
          updateQuizH5PAnswerFields();
          updateQuizH5PScoreField();
        });
      }
    }
  };

  Drupal.behaviors.quizH5PIntegrationQuestion = {
    attach: function (context, settings) {
      if (window.H5P) {
        // In case of ajax quiz questions we reload the H5P question.
        if (settings.quizH5PIntegration && settings.quizH5PIntegration.H5PIntegration) {
          window.H5PIntegration = settings.quizH5PIntegration.H5PIntegration;
        }
        else {
          if (window.H5PIntegration) {
            window.H5PIntegration = window.H5PIntegration;
          }
        }
        if (window.H5PIntegration) {
          window.H5P.init();
        }

        // Triggering init() function when DOM is ready.
        $(document).ready(function () {
          init();
        });

        // Triggering loadUserAnswers() function when document is loaded.
        $(window).load(function () {
          // Loads old answers if this quiz was already taken.
          loadUserAnswers();
        });
      }
    }
  };

  // Run drupal's attachBehaviors() just to be sure that quizH5PIntegrationQuestion behavior is loaded.
  Drupal.attachBehaviors(document, Drupal.settings);

})(jQuery, Drupal, this, this.document);
