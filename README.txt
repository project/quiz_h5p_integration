CONTENTS OF THIS FILE
---------------------

 * Overview
 * Features
 * Requirements
 * Installation
 * Known Problems
 * Documentation

OVERVIEW
--------

This module is a complete rewrite of the Quiz H5P module with a better
integration between the Quiz module and the H5P module.

FEATURES
--------

Makes H5P question types available in Quiz module and adds specific quiz
options to H5P question types (binary score). For some H5P questions :
 * calculates automatically the quiz taker max score;
 * validates his answers against H5P valid answers stored in database.

REQUIREMENTS
------------

 * Drupal 7.

This module requires the following modules:

 * Quiz 7.x-5.x (https://www.drupal.org/project/quiz)
 * H5P 7.x-1.x (https://www.drupal.org/project/h5p)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

KNOW PROBLEMS
-------------

 * No reports yet.

DOCUMENTATION
-------------

 * Check out https://www.drupal.org/sandbox/virtuoworks/2729985
