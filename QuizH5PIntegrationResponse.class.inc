<?php

/**
 * @file
 * QuizH5PIntegrationResponse.class.php file.
 *
 * @category Quiz Question
 *
 * @package Quiz H5P Integration
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

/**
 * QuizH5PIntegrationResponse class.
 *
 * Extension of QuizQuestionResponse.
 * Inherits or implements code found in quiz_question.core.inc.
 */
class QuizH5PIntegrationResponse extends QuizQuestionResponse {

  /**
   * Constructor.
   *
   * @param int $result_id
   *   The result ID for the user's result set. There is one result ID per time
   *   the user takes a quiz.
   * @param object $question_node
   *   The question node.
   * @param mixed $answer
   *   The answer (dependent on question type).
   *
   * @override
   *   QuizResponse constructor stores a new user response.
   */
  public function __construct($result_id, stdClass $question_node, $answer = NULL) {
    parent::__construct($result_id, $question_node, $answer);
    if (isset($answer)) {
      $this->evaluated = FALSE;
      $this->score = (isset($answer['quiz_h5p_integration_user_score']) ? $answer['quiz_h5p_integration_user_score'] : 0);
      if (isset($answer['quiz_h5p_integration_user_answers'])) {
        $this->answer = json_encode($answer['quiz_h5p_integration_user_answers']);
      }
      else {
        $this->answer = NULL;
      }
    }
    else {
      $efq = new EntityFieldQuery();
      $res = $efq->entityCondition('entity_type', 'quiz_h5p_integration_user_answers')
        ->propertyCondition('result_answer_id', $this->result_answer_id)
        ->range(0, 1)
        ->execute();

      if (!empty($res['quiz_h5p_integration_user_answers'])) {
        if ($quiz_h5p_integration_user_answers = current($res['quiz_h5p_integration_user_answers'])) {
          $quiz_h5p_integration_user_answer = array();
          if (!empty($quiz_h5p_integration_user_answers->answer_id)) {
            $quiz_h5p_integration_user_answer = current(entity_load('quiz_h5p_integration_user_answers', array($quiz_h5p_integration_user_answers->answer_id)));
          }
        }
      }

      if (!empty($quiz_h5p_integration_user_answer)) {
        $this->evaluated = TRUE;
        $this->score = $quiz_h5p_integration_user_answer->score;
        $this->answer = $quiz_h5p_integration_user_answer->answer;
        $this->answer_id = $quiz_h5p_integration_user_answer->answer_id;
      }
    }
  }

  /**
   * Save the response for this question response.
   */
  public function save() {
    db_merge('quiz_h5p_integration_user_answers')
      ->key(array('result_answer_id' => $this->result_answer_id))
      ->fields(array(
        'answer' => $this->answer,
        'score' => $this->getScore(FALSE),
        'result_answer_id' => $this->result_answer_id,
      ))
      ->execute();
  }

  /**
   * Delete the response for this question response.
   */
  public function delete() {
    db_delete('quiz_h5p_integration_user_answers')
      ->condition('result_answer_id', $this->result_answer_id)
      ->execute();
  }

  /**
   * Calculate the unscaled score in points for this question response.
   */
  public function score() {
    $score = 0;
    $user_answers = array();

    if (isset($this->answer) && is_string($this->answer)) {
      $user_answers = json_decode($this->answer);
    }

    if (isset($this->question->valid_answers)) {
      $valid_answers = $this->question->valid_answers;
    }
    else {
      $valid_answers = array();
    }

    if (is_array($valid_answers) && count($valid_answers)) {
      if (is_array($user_answers) && count($valid_answers) === count($user_answers)) {
        if (count($valid_answers)) {
          foreach (array_values($valid_answers) as $index => $valid_answer) {
            if (empty($user_answers[$index])) {
              $score = 0;
              break;
            }

            if (!empty($valid_answer['answer']['value'])) {
              $valid_answer_value = json_decode($valid_answer['answer']['value']);
              if (is_array($valid_answer_value) && in_array($user_answers[$index], $valid_answer_value)) {
                $score++;
              }
              else {
                if ($valid_answer_value == $user_answers[$index]) {
                  $score++;
                }
              }
            }
          }

          if (!$this->isEvaluated()) {
            if (!(isset($this->score) && is_numeric($this->score) && $this->score == $score)) {
              $score = 0;
            }
          }
        }
        else {
          if (isset($this->score) && is_numeric($this->score)) {
            $score = $this->score;
          }
        }
      }
    }
    else {
      if (isset($this->score) && is_numeric($this->score)) {
        $score = $this->score;
      }
    }

    if ($score) {
      $maximum_score = $this->getQuizQuestion()->getMaximumScore(TRUE);
      if (is_numeric($maximum_score)) {
        if (isset($this->question->choice_boolean) && $this->question->choice_boolean) {
          if ($score == $maximum_score) {
            $score = 1;
          }
          else {
            $score = 0;
          }
        }
        else {
          if ($score > $maximum_score) {
            $score = 0;
          }
        }
      }
      else {
        $score = 0;
      }
    }

    return $score;
  }

  /**
   * Get the user's response.
   */
  public function getResponse() {
    if (empty($this->answer)) {
      return NULL;
    }
    else {
      return $this->answer;
    }
  }

  /**
   * Sets the feedback values.
   *
   * @override
   *   Get the response part of the report form
   *
   * @return array
   *   Array of response data, with each item being an answer to a response. For
   *   an example, see MultichoiceResponse::getReportFormResponse(). The sub
   *   items are keyed by the feedback type. Providing a NULL option means that
   *   feedback will not be shown. Seen an example at
   *   LongAnswerResponse::getReportFormResponse().
   */
  public function getFeedbackValues() {
    $data = array();

    $user_attempt = NULL;
    if (!empty($this->question->answers[0]['answer'])) {
      $user_answers = json_decode($this->question->answers[0]['answer']);
      if (is_array($user_answers) && !empty($user_answers)) {
        $user_attempt = htmlentities('«' . implode('», «', $user_answers) . '»', ENT_QUOTES, 'UTF-8');
      }
    }
    $question_feedback = NULL;
    if (!empty($this->question->feedback['value']) && !empty($this->question->feedback['format'])) {
      $question_feedback = check_markup($this->question->feedback['value'], $this->question->feedback['format']);
    }
    $answer_feedback = NULL;
    $solution = NULL;
    if (!empty($this->question->valid_answers) && is_array($this->question->valid_answers)) {
      foreach ($this->question->valid_answers as $valid_answer) {
        $solution .= $valid_answer['feedback']['value'] . ', ';
      }
      $solution = trim($solution, ', ');
    }
    $quiz_feedback = NULL;

    $data[] = array(
      'choice' => NULL,
      'attempt' => $user_attempt,
      'correct' => $this->question->answers[0]['is_correct'] ? quiz_icon('correct') : quiz_icon('incorrect'),
      'score' => !$this->evaluated ? t('This answer has not yet been scored.') : $this->getScore(),
      'answer_feedback' => $answer_feedback,
      'question_feedback' => $question_feedback,
      'solution' => $solution,
      'quiz_feedback' => $quiz_feedback,
    );

    return $data;
  }

  /**
   * Implementation of getReportFormSubmit.
   *
   * @see QuizQuestionResponse#getReportFormSubmit()
   */
  public function getReportFormSubmit() {
    // TODO !
    // return 'quiz_h5p_integration_report_submit';
    // quiz_h5p_integration_report_submit is a function in module file.
    return FALSE;
  }

  /**
   * Implementation of getReportFormAnswerFeedback.
   *
   * @see QuizQuestionResponse#getReportFormAnswerFeedback()
   */
  public function getReportFormAnswerFeedback() {
    return FALSE;
  }

}
